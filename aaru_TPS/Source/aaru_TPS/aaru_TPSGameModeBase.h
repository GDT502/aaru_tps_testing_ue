// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "aaru_TPSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AARU_TPS_API Aaaru_TPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
