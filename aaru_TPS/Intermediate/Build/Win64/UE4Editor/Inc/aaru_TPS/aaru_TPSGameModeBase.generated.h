// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AARU_TPS_aaru_TPSGameModeBase_generated_h
#error "aaru_TPSGameModeBase.generated.h already included, missing '#pragma once' in aaru_TPSGameModeBase.h"
#endif
#define AARU_TPS_aaru_TPSGameModeBase_generated_h

#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_SPARSE_DATA
#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_RPC_WRAPPERS
#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAaaru_TPSGameModeBase(); \
	friend struct Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics; \
public: \
	DECLARE_CLASS(Aaaru_TPSGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/aaru_TPS"), NO_API) \
	DECLARE_SERIALIZER(Aaaru_TPSGameModeBase)


#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAaaru_TPSGameModeBase(); \
	friend struct Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics; \
public: \
	DECLARE_CLASS(Aaaru_TPSGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/aaru_TPS"), NO_API) \
	DECLARE_SERIALIZER(Aaaru_TPSGameModeBase)


#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aaaru_TPSGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aaaru_TPSGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aaaru_TPSGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aaaru_TPSGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aaaru_TPSGameModeBase(Aaaru_TPSGameModeBase&&); \
	NO_API Aaaru_TPSGameModeBase(const Aaaru_TPSGameModeBase&); \
public:


#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aaaru_TPSGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aaaru_TPSGameModeBase(Aaaru_TPSGameModeBase&&); \
	NO_API Aaaru_TPSGameModeBase(const Aaaru_TPSGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aaaru_TPSGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aaaru_TPSGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aaaru_TPSGameModeBase)


#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_12_PROLOG
#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_SPARSE_DATA \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_RPC_WRAPPERS \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_INCLASS \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_SPARSE_DATA \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AARU_TPS_API UClass* StaticClass<class Aaaru_TPSGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID aaru_TPS_Source_aaru_TPS_aaru_TPSGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
