// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "aaru_TPS/aaru_TPSGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeaaru_TPSGameModeBase() {}
// Cross Module References
	AARU_TPS_API UClass* Z_Construct_UClass_Aaaru_TPSGameModeBase_NoRegister();
	AARU_TPS_API UClass* Z_Construct_UClass_Aaaru_TPSGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_aaru_TPS();
// End Cross Module References
	void Aaaru_TPSGameModeBase::StaticRegisterNativesAaaru_TPSGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_Aaaru_TPSGameModeBase_NoRegister()
	{
		return Aaaru_TPSGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_aaru_TPS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "aaru_TPSGameModeBase.h" },
		{ "ModuleRelativePath", "aaru_TPSGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aaaru_TPSGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::ClassParams = {
		&Aaaru_TPSGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aaaru_TPSGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aaaru_TPSGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aaaru_TPSGameModeBase, 3888033163);
	template<> AARU_TPS_API UClass* StaticClass<Aaaru_TPSGameModeBase>()
	{
		return Aaaru_TPSGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aaaru_TPSGameModeBase(Z_Construct_UClass_Aaaru_TPSGameModeBase, &Aaaru_TPSGameModeBase::StaticClass, TEXT("/Script/aaru_TPS"), TEXT("Aaaru_TPSGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aaaru_TPSGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
